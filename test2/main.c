#include <stdio.h>
#include <stdlib.h>

int main()
{
    int grade;
    printf("Enter grade:  ");
    scanf("%d", &grade);
    if (100 >= grade >= 90)
    printf("Excellent");
    else if (90 > grade >= 80)
    printf("Very good");
    else if (80 >= grade >= 70)
    printf("Good");
    else if (70 >= grade >= 60)
    printf("Pass");
    else if (0 =< grade < 60)
    printf("Fail");
    else
        printf("Enter a grade between 0 and 100");
    return 0;
}
